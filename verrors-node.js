const
  chalk = require(`chalk`),
  cleanStack = require(`clean-stack`);

class Verror extends Error {
  constructor(missive) {
    super();

    this.name = ``;
    this.title = `Orpin: ${this.constructor.name}`;
    this.missive = missive;
    this.stack = cleanStack(this.stack);
  }

  display() {
    return `[${chalk.red(this.title)}]: ${chalk.yellow(this.missive)}\n${chalk.blue(this.stack)}`;
  }
}

module.exports = Verror;
