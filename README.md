# verrors-node [![npm version](https://badge.fury.io/js/%40vaemoi%2Fverrors-node.svg)](https://badge.fury.io/js/%40vaemoi%2Fverrors-node)
[![Known Vulnerabilities](https://snyk.io/test/npm/@vaemoi/verrors-node/badge.svg)](https://snyk.io/test/npm/@vaemoi/verrors-node)
[![Maintainability](https://api.codeclimate.com/v1/badges/4700cd17df320f599eba/maintainability)](https://codeclimate.com/repos/5d59818a0d57e0009d001070/maintainability)
[![Coverage Status](https://coveralls.io/repos/gitlab/vaemoi/tooly/verrors-node/badge.svg?branch=master)](https://coveralls.io/gitlab/vaemoi/tooly/verrors-node?branch=master)

Custom NodeJS error class

## Usage
```js

// extend the Verror class to your liking to describe errors in your code
const Verror = require(`@vaemoi/verrors-node`);

class BaseError extends Verror {}


class FetchError extends BaseError {
  constructor(message, code, url) {
    super(`Problem with request -- ${message}: ${code}\n\t${url}`);
  }
}

class BadURLError extends FetchError {
  constructor(code, url) {
    super(`probably a bad url`, code, url);
  }
}

class ServerError extends FetchError {
  constructor(code, url) {
    super(`server error`, code, url);
  }
}

// Catch errors and print them using .display()
...
try {
    // Fetching code ...

    if (!response.ok) {
        const err = response.status < 500 ? OrpinError.BadURL : OrpinError.Server;

        throw new err(response.status, info.cdnURI);
    }
} catch (err) {
    if (err instanceof Verror) {
        console.log(err.display());
    } else {
        console.error(err);
    }
}


```