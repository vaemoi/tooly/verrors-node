const test = require(`tape`);

const Verror = require(`../verrors-node.js`);

test(`Should create custom error class`, (swear) => {
  class SomeError extends Verror {
    constructor() {
      super(`This is just a test`);
    }
  }

  const someErrorSpewer = () => {
    throw new SomeError;
  };

  swear.plan(3);

  swear.throws(someErrorSpewer, SomeError, `Throws custom error`);
  swear.throws(someErrorSpewer, Verror, `Custom error inherits from Error`);

  const err = new SomeError;
  const maybeErrorMessage = `[${err.title}]: ${err.missive}\n${err.stack}`;

  const errorMessage = err.display();

  swear.ok(errorMessage === maybeErrorMessage, `Displays expected error message`);
});
